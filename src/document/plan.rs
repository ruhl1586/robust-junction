use std::collections::{HashMap, HashSet};
use crate::document::interlocking::*;
use rolling::input::staticinfrastructure as rolling_inf;
use crate::document::model::*;
use crate::document::history;
use crate::document::dgraph::{DGraph, edge_length};
use rolling::output::history::*;
use nalgebra::max;
use std::hash::Hash;
use rolling::input::staticinfrastructure::NodeId;

#[derive(Debug)]
pub enum ConvertPlanErr {
    VehicleRefMissing,
    VehicleMissing,
}

pub enum TestPlanErr {
    MissingVisits,
    MissingTrain,
    // First number is the max time spec, second is actual time needed between visits
    TimeOverrun(Vec<(VisitRef, VisitRef, f64, f64)>),
    VisitOrderError,
    TimingError,
}

#[derive(Debug)]
pub struct SimRobustnessEvaluation {
    pub number_of_runs: usize,
    pub punctuality: f64,
    pub confidence_interval: (f64, f64),
    pub ttq_per_constraint: HashMap<usize, f64>
}

pub struct OptimalTTQs {
    pub ttq_per_constraint: HashMap<usize, f64>
}

impl SimRobustnessEvaluation {
    pub(crate) fn default() -> SimRobustnessEvaluation {
        SimRobustnessEvaluation {
            number_of_runs: 1,
            punctuality: 1.0,
            confidence_interval: (1.0, 1.0),
            ttq_per_constraint: HashMap::new()
        }
    }
}

pub fn eval_plan(dgraph :&DGraph, plan_spec :&PlanSpec, history :&History) -> Result<HashMap<usize, (VisitRef, VisitRef, f64, f64)>, TestPlanErr> {

    // Record each visit's time for checking the ordering constraints.
    let mut visit_times : HashMap<VisitRef, f64> = HashMap::new();

    // 1. check train's visits
    for (train_idx, (train_id, (veh, visits))) in plan_spec.trains.iter().enumerate() {
        let mut t = 0.0;
        let mut current_visit = 0;
        let (train_name, train_params, train_log) = history.trains.get(train_idx).ok_or(TestPlanErr::MissingTrain)?;
        for ev in train_log.iter() {

            if let TrainLogEvent::Wait(dt) = ev { 
                t += *dt;
            }
            if let TrainLogEvent::Move(dt,_,_) = ev { 
                t += *dt;
            }

            if !(current_visit < visits.data().len()) { 
                break;
            }

            if event_matches_spec(dgraph, &visits.data()[current_visit].1, ev) {
                visit_times.insert((*train_id, visits.data()[current_visit].0), t);
                current_visit += 1;
            }
        }
        if current_visit < visits.data().len() {
            // train train_idx failed to reach its planned locations
            return Err(TestPlanErr::MissingVisits); 
        }
    }


    // 2. check ordering constraints and time diff
    let mut overruns: Vec<(VisitRef, VisitRef, f64, f64)> = Vec::new();
    let mut constraint_times: HashMap<usize, (VisitRef, VisitRef, f64, f64)> = HashMap::new();

    for (id, (ra,rb,dt)) in plan_spec.order.iter() {
        let t1 = visit_times.get(ra).ok_or(TestPlanErr::VisitOrderError)?;
        let t2 = visit_times.get(rb).ok_or(TestPlanErr::VisitOrderError)?;

        // Visits happen in order
        // Commented out by Carl, works without the check & this check discards valid solutions
        // probably because of rounding errors in the time stamps
        // if !(t1 <= t2) {
        //     return Err(TestPlanErr::TimingError);
        // }

        // Return all timing constraints for fulfillment checking
        if let Some(dt) = dt {
            constraint_times.insert(*id, (*ra, *rb, *dt, t2-t1));
        }
    }


    Ok(constraint_times)
}

pub fn test_plan(dgraph :&DGraph,
                 il :&Interlocking,
                 vehicles :&[(usize,Vehicle)],
                 plan_spec :&PlanSpec,
                 candidate :&planner::input::RoutePlan, )
                 -> Result<Result<(Commands, History, SimRobustnessEvaluation, HashMap<usize, f64>),TestPlanErr>,String> {
    let commands = convert_dispatch_commands(candidate, il, plan_spec)?;

    // HashMap collecting all possible visit locations for each train
    // to give to the driver instance for delay induction
    let mut train_visits: HashMap<usize, Vec<usize>> = HashMap::new();

    // Dummy HashMap for (train -> (visit -> distance)) mapping to give to the simulation without delay
    let mut dummy_train_distances : HashMap<usize, HashMap<NodeId, f64>> = HashMap::new();

    for (train_id, visits) in plan_spec.trains.iter() {
        let mut train_locations : Vec<usize> = Vec::new(); // the locations this train can or must visit
        dummy_train_distances.insert(*train_id, HashMap::new()); // insert empty HashMap for each train

        // walk through all locs in all visits of this train
        for (visit_id, visit) in visits.1.iter() {
            for location in visit.locs.iter() {
                // Get the ID of the location and add it to the location list of this train
                if let Ok(Ref::Node(pt)) = &location { // specified location is a node
                    // have to check both boundaries and switch nodes in the dgraph

                    // Some hacking with node IDs to ensure that both ids of a double node are recognized,
                    // independent of the travelling direction.
                    // NodeID = 0 is outside the model, for every other node
                    if let Some(node_id) = dgraph.node_ids.get_by_right(pt) {
                        train_locations.push(*node_id);
                        if (*node_id>1) {train_locations.push(*node_id-1)} else {train_locations.push(*node_id+1)};
                    } else if let Some(node_id)  = dgraph.switch_ids.get_by_right(pt) {
                        train_locations.push(*node_id);
                        if (*node_id>1) {train_locations.push(*node_id-1)} else {train_locations.push(*node_id+1)};
                    }
                } // Same if the location is a detector
                else if let Ok(Ref::Object(pt)) = &location { // Specified Visit is a detector
                    if let Some(node_id) = dgraph.detector_ids.get_by_right(pt)
                    {
                        //TODO @Carl ugly ugly hack: Detector IDs appear to be 1 higher during
                        // simulation than here, so this node_id+1 is the workaround
                        //train_locations.push(*node_id);
                        train_locations.push(*node_id+1);
                    }
                }
            }
        }
        train_visits.insert(*train_id, train_locations);
    }

    // simulate once without stochastic delay
    let (history, route_refs) =
        history::get_history(vehicles, &dgraph.rolling_inf, il, &commands, &train_visits, false, dummy_train_distances)?;
    // check if all constraints are fulfilled in the optimal case
    let simulation_result = eval_plan(dgraph, plan_spec, &history);

    // check that the ordering of the plan is satisfied
    if let Err(e) = simulation_result { return Ok(Err(e)); }

    // build the HashMap for TTQs in the optimal case
    let mut ttqs_optimal : HashMap<usize, f64> = HashMap::new();

    //return an err if any timing constraint was violated
    for (id, (visit1, visit2, time_bound, actual_time)) in simulation_result.unwrap_or(HashMap::new()) {
        if plan_spec.with_timing && time_bound <= actual_time {
            //print!("Timing constraint {} was violated without delay\n", id);
            return Ok(Err(TestPlanErr::TimingError));
        }
        ttqs_optimal.insert(id, actual_time/time_bound);

    }

    // Build the HashMap of (Visit, Length) for each train
    let train_trace = get_orderered_nodes(&history);

    let mut train_visits_with_distances : HashMap<usize, HashMap<NodeId, f64>> = HashMap::new();
    for (train_id, nodes) in train_trace {
        train_visits_with_distances.insert(train_id, get_visits_with_distances(
            nodes,
            train_visits.get(&train_id).expect("Wrong train ID"),
            dgraph));
    }

    //print!("Train IDs with Visit, distances: {:?}\n", train_visits_with_distances);


    // init variables for robustness evaluation
    let mut number_of_timed_constraints = 0;
    let mut punctual_constraints = 0;
    let mut punctuality_per_run: Vec<f64> = Vec::new();
    let mut constraint_sums: HashMap<usize, (f64, usize, f64)> = HashMap::new(); //id -> (actual time, number of sims, time bound)
    let mut constraint_ttqs: HashMap<usize, f64> = HashMap::new(); // id -> ttq

    // Begin monte carlo sim loop
    for run in 0..plan_spec.number_of_runs {

        // simulate the dispatch
        let (history, route_refs) =
            history::get_history(
                vehicles,
                &dgraph.rolling_inf,
                il,
                &commands,
                &train_visits,
                true,
                train_visits_with_distances.clone()
            )?;
        let simulation_result = eval_plan(dgraph, plan_spec, &history);

        // check that the ordering of the plan is satisfied
        if let Err(e) = simulation_result {
            match e {
                TestPlanErr::VisitOrderError => print!("Constrained Visit not found\n"),
                TestPlanErr::TimingError => print!("Ordering constraint not fulfilled\n"),
                _ => {}
            }
            return Ok(Err(e));
        }

        let mut current_constaint_number = 0;
        let mut current_punctual_constraints = 0;

        // count the fulfilled timing constraints
        for (id, (visit1, visit2, time_bound, actual_time)) in simulation_result.unwrap_or(HashMap::new()) {
            current_constaint_number += 1;
            number_of_timed_constraints += 1;
            if time_bound >= actual_time {
                current_punctual_constraints += 1;
                punctual_constraints += 1;
            }

            // add up the sum of times needed for each constraint
            // and the number of times this constraint was simulated
            if constraint_sums.contains_key(&id) {
                constraint_sums.get_mut(&id).expect("Invalid Constraint id").0 += actual_time;
                constraint_sums.get_mut(&id).expect("Invalid Constraint id").1 += 1;
            } else { // create new entry for this constraint
                constraint_sums.insert(id, (actual_time, 1, time_bound));
            }
        }

        punctuality_per_run.push(current_punctual_constraints as f64/current_constaint_number as f64);



        // calculate the time travel quotient for each constraint
        for (id, (sum, runs, time_bound)) in &constraint_sums {
            let ttq = (sum/(*runs as f64))/time_bound;
            constraint_ttqs.insert(*id, ttq);
        }
    }
    let punctuality: f64 = punctual_constraints as f64/number_of_timed_constraints as f64;
    let standard_deviation = get_standard_deviation(punctuality, punctuality_per_run, plan_spec.number_of_runs as f64);
    let confidence_interval = get_confidence_interval_95(punctuality, standard_deviation, plan_spec.number_of_runs as f64);


    //print!("Number of timing constraints: {}, number of punctual constraints: {}\n", number_of_timed_constraints, punctual_constraints);
    //print!("Punctuality: {}\n\n", punctuality);


    let evaluation = SimRobustnessEvaluation{
        number_of_runs: plan_spec.number_of_runs,
        punctuality: (punctual_constraints as f64)/(number_of_timed_constraints as f64),
        confidence_interval: confidence_interval,
        ttq_per_constraint: constraint_ttqs
    };


    // End sim loop
    // Evaluate simulation results
    return Ok(Ok((commands,history, evaluation, ttqs_optimal)));
}


fn event_matches_spec(dgraph :&DGraph, visit :&Visit, event :&TrainLogEvent) -> bool {
    if let TrainLogEvent::Node(n) = event { // Node Spec or Detector Spec
        //print!("Event: {:?}\n", event);
        for l in visit.locs.iter() {
            match l {
                Ok(Ref::Node(pt)) => { // specified visit is a node
                    // have to check both boundaries and switch nodes in the dgraph
                    if dgraph.node_ids.get_by_left(n) == Some(pt) {
                        //print!("Node {} visited at time ", n);
                        return true;
                    }
                    if dgraph.switch_ids.get_by_left(n) == Some(pt) {
                        //print!("Node {} visited at time ", n);
                        return true;
                    }
                },
                Ok(Ref::Object(pt)) => { // Specified Visit is a detector
                    if dgraph.detector_ids.get_by_left(n) == Some(pt) {
                        //print!("Detector Node {} visited at time ", n);
                        return true;
                    }
                }
                _ => {}, //unimplemented!(), // TODO
            };
        }
    } else if let TrainLogEvent::Sight(n, true) = event { // Sightline Spec
        // Whether a Signal Visit was met is determined by whether the driver has seen
        // the specified signal. Since the driver sees only the next signal and only
        // the signals he will drive past, this is equivalent to specifying that
        // the train should pass that signal.
        for l in visit.locs.iter() {
            match l {
                Ok(Ref::Object(ptA)) => { // Object in sightline is a signal
                    // the signal is the same as in the specification
                    if dgraph.object_ids.get_by_left(n) == Some(ptA) {
                        return true;
                    }
                }
                _ => {}, //unimplemented!(), // TODO
            };
        }
    }
    false
}

/// Calculate the standard deviation from a number of samples
fn get_standard_deviation(mean: f64, samples: Vec<f64>, n: f64) -> f64 {
    let mut sum = 0.0;
    for sample in samples {
        sum += (sample - mean) * (sample - mean);
    }

    return (1.0/(n-1.0) * sum).sqrt();
}

/// Return the 95% confidence interval of the estimated mean punctuality
/// mean: The mean of the samples
/// std_dev: The standard deviation of the samples
/// n: The number of samples
fn get_confidence_interval_95(mean: f64, std_dev: f64, n: f64) -> (f64, f64) {
    let p_1 = mean - 1.96*(std_dev)/(n.sqrt());
    let p_2 = mean + 1.96*(std_dev)/(n.sqrt());

    (p_1, p_2)
}

/// For each train in the history return a HashMap with the ordered list of nodes each train passed
fn get_orderered_nodes(history: &History) -> HashMap<usize, Vec<NodeId>> {
    let mut train_routes : HashMap<usize, Vec<NodeId>> = HashMap::new();
    for (train_idx, (train_name, train_params, train_log)) in history.trains.iter().enumerate() {
        train_routes.insert(train_idx, Vec::new());
        for event in train_log {
            match event {
                // If a Node was passed insert this node to this trains list of passed nodes
                TrainLogEvent::Node(node_id) =>  train_routes.get_mut(&train_idx)
                    .expect("Train ID invalid").push(*node_id),
                _ => {}
            }
        }
    }
    train_routes
}

/// For a list of visited Nodes and a List of
fn get_visits_with_distances(all_nodes: Vec<NodeId>, visits: &Vec<NodeId>, dgraph: &DGraph) -> HashMap<NodeId, f64> {
    let mut visits_with_distances: HashMap<NodeId, f64> = HashMap::new();

    // Go through all Nodes that should be visited
    for (position, current_visit) in visits.iter().enumerate() {

        // Multiple if lets because some locations are not visited (are in one visit or NodeId is different)
        if let Some(next_visit) = visits.get(position+1) {
            if let Some(current_visit_position) = all_nodes.iter().position(|&x| x == *current_visit) {
                if let Some(next_visit_position) = all_nodes.iter().position(|&x| x == *next_visit) {
                    let mut travelled_nodes: Vec<NodeId> = vec![];

                    // The current and the next visit are now found, read the path between them
                    for (position, node) in all_nodes.iter()
                        .enumerate()
                        .filter(|&(i, _)| (i >= current_visit_position && i <= next_visit_position)) {
                        travelled_nodes.push(*node);
                    }
                    // calculate the total distance of the path and store it for this visit
                    visits_with_distances.insert(*current_visit, get_travel_distance(travelled_nodes, &dgraph));
                }
            }
        }
    }


    visits_with_distances
}

/// Get the distance travelled when taking the route through all the given Nodes
fn get_travel_distance(travelled_nodes: Vec<NodeId>, dgraph: &DGraph) -> f64 {
    let mut distance = 0.0;
    for (index, node) in travelled_nodes.iter().enumerate() {
        if let Some(next_node) = travelled_nodes.get(index+1) {
            distance += edge_length(&dgraph.rolling_inf, *node, *next_node).unwrap_or(0.0);
        }
    }
    distance
}

pub fn get_dispatches(
      dgraph :&DGraph,
      il :&Interlocking, 
      vehicles :&[(usize,Vehicle)],
      plan :&PlanSpec,
      ) -> Result<Vec<(Dispatch, History, SimRobustnessEvaluation, HashMap<usize, f64>)>, String> {

    let routes : HashMap<usize,rolling_inf::Route> = 
        il.routes.iter().map(|r| r.route.clone()).enumerate().collect();
    let route_specs : HashMap<usize,RouteSpec> = 
        il.routes.iter().map(|r| r.id.clone()).enumerate().collect();

    let plan_inf = convert_inf(&routes);
    let plan_usage = convert_plan(il, vehicles, plan).
        map_err(|e| format!("{:?}", e))?;
    let config = planner::input::Config {
        n_before: 3, n_after: 3, exact_n: None, optimize_signals: false,
    };

    //println!(" STARTIN GPLANNIGN");
    //println!("infrastructure {:#?}", plan_inf);
    //println!("usage {:#?}", plan_usage);

    let mut output = Vec::new();
    planner::solver::plan(&config, &plan_inf, &plan_usage, |candidate| {
        //println!("got one plan");
        if let Ok((cmds,p, evaluation, optimal_ttqs)) = test_plan(dgraph, il, vehicles, plan, candidate).unwrap() {
            let name = format!("Dispatch {}", output.len()+1);
            output.push((Dispatch::from_vec(name,cmds),p, evaluation, optimal_ttqs));
        }
        false // always return false to ensure that the planner will explore further, even if plan was successful
    });
    //println!("planner finished");
    Ok(output)
}


fn convert_dispatch_commands(routeplan :&planner::input::RoutePlan, il :&Interlocking,
                          plan :&PlanSpec) -> Result<Commands,String> {

    use std::collections::BTreeSet;

    let mut commands = Vec::new();
    let mut last_active_routes = BTreeSet::new();

    for state in routeplan.iter() {
        let active_routes = state.iter().filter_map(|((elementary,part),train_id)| {
            // use partial as representative for elementary route
            if *part == 0 && train_id.is_some() {
                Some((*elementary, train_id.unwrap())) 
            } else { None }
        }).collect::<BTreeSet<_>>();

        for (new_route, train_id) in active_routes.difference(&last_active_routes) {
            // check if the route is in the birth of a train (comes from boundary)
            match il.routes[*new_route].route.entry {
                rolling_inf::RouteEntryExit::Boundary(_) => {
                    // Spawn new train
                    commands.push((0.0, Command::Train(
                                plan.trains.get(*train_id).unwrap().0.unwrap(), //vehicle id
                                il.routes[*new_route].id)));
                },
                rolling_inf::RouteEntryExit::Signal(_) 
                    | rolling_inf::RouteEntryExit::SignalTrigger { .. } => {
                        commands.push((0.0, Command::Route(il.routes[*new_route].id)));
                },
            }
        }

        last_active_routes = active_routes;
    }

    Ok(commands.into_iter().enumerate().collect())
}


pub fn convert_inf(routes :&rolling_inf::Routes<usize>) -> planner::input::Infrastructure {

    let mut partial_routes = HashMap::new();
    let mut elementary_routes = Vec::new();
    let mut partial_route_resources :HashMap<usize, HashSet<planner::input::PartialRouteId>> = HashMap::new();
    let mut fresh = { let mut i = 0; move || { i += 1; i } };

    fn convert_routeentryexit(e :&rolling_inf::RouteEntryExit) -> planner::input::SignalId {
        match e {
            rolling_inf::RouteEntryExit::Boundary(_) => planner::input::SignalId::Boundary,
            rolling_inf::RouteEntryExit::Signal(signal) |
            rolling_inf::RouteEntryExit::SignalTrigger { signal, .. } => 
                planner::input::SignalId::Signal(*signal),
        }
    }

    let mut boundary_routes :HashMap<rolling_inf::NodeId, HashSet<usize>> = HashMap::new();
    let mut route_boundaries :Vec<(planner::input::PartialRouteId, 
                                   planner::input::PartialRouteId, 
                                   planner::input::SignalId)> = Vec::new();
    for (route_name,route) in routes.iter() {
        let mut signals = vec![convert_routeentryexit(&route.entry)];
        for i in 0..(route.resources.releases.len()-1) {
            // Add each release's end/exit detector, except the 
            // last one, which has the route exit signal at its end.
            if let Some(end_node) = route.resources.releases[i].end_node {
                signals.push(planner::input::SignalId::Detector(end_node));
            } else {
                signals.push(planner::input::SignalId::Detector(1_000_000_000 + fresh()));
            }
        }
        signals.push(convert_routeentryexit(&route.exit));

        for s in &[&route.entry,&route.exit] {
            if let rolling_inf::RouteEntryExit::Boundary(Some(n)) = s {
                boundary_routes.entry(*n).or_insert(HashSet::new()).insert(*route_name);
            }
        }

        let mut elementary_route = HashSet::new();
        for (i,(entry,exit)) in signals.iter().zip(signals.iter().skip(1)).enumerate() {
            let (length,resources) = if route.resources.releases.len() > 0 {
                let release = route.resources.releases[i].clone();
                (release.length, release.resources)
            } else {
                (route.length, std::iter::empty().collect())
            };

            partial_routes.insert((*route_name, i), planner::input::PartialRoute {
                entry: *entry, exit: *exit,
                conflicts: Vec::new(),
                wait_conflict: None,
                length: length as _,
            });

            for resource in resources.iter() {
                partial_route_resources.entry(*resource)
                    .or_insert(HashSet::new())
                    .insert((*route_name, i));
            }
            elementary_route.insert((*route_name, i));
        }
        elementary_routes.push(elementary_route);
    }

    // second pass adds conflicting routes from resource -> partialroutes map
    for (rn,r) in routes.iter() {
        if r.resources.releases.len() > 0 {
            for (i,rel) in r.resources.releases.iter().enumerate() {
                let mut conflicting_routes = HashSet::new();
                for resource in rel.resources.iter() {
                    if let Some(conflicts) = partial_route_resources.get(resource) {
                        conflicting_routes.extend(conflicts.iter().cloned()
                                                  .filter(|(pr_e,pr_p)| pr_e != rn)
                                                  .map(|pr| (pr,0)));
                    }
                }

                partial_routes.get_mut(&(*rn,i)).unwrap().conflicts =
                    vec![conflicting_routes]; // TODO overlap alternatives
            }
        } else {
            // There are no resources, but we have to add the overlap choice anyway
            partial_routes.get_mut(&(*rn,0)).unwrap().conflicts = 
                vec![std::iter::empty().collect()];
        }
    }

    // Add boundary conflicts
    for (_, set) in boundary_routes {
        //println!("Exlcuding set of routes because they share a boundary: {:?}", set);
        let set :Vec<usize> = set.into_iter().collect();
        for (i,j) in set.iter().flat_map(|x| set.iter().map(move |y| (*x,*y))).filter(|(x,y)| x != y) { 
            let j_choices = partial_routes.get_mut(&(j,0)).unwrap().conflicts.len();
            
            for cs in partial_routes.get_mut(&(i,0)).unwrap().conflicts.iter_mut() {
                for choice in 0..j_choices {
                    cs.insert(((j,0),choice));
                }
            }
        }
    }


    planner::input::Infrastructure { partial_routes, elementary_routes }
}


pub fn convert_plan(il :&Interlocking, 
                    vehicles :&[(usize,Vehicle)], 
                    plan :&PlanSpec, ) -> Result<planner::input::Usage, ConvertPlanErr> {

    let mut trains = HashMap::new();

    // @Carl go through all trains
    for (t_id,(vehicle_id,visits)) in plan.trains.iter() {
        let vehicle_id = vehicle_id.ok_or(ConvertPlanErr::VehicleRefMissing)?;
        let vehicle = vehicles.iter().find(|(i,v)| *i == vehicle_id).map(|(i,v)| v)
            .ok_or(ConvertPlanErr::VehicleMissing)?;
        let mut planner_visits :Vec<HashSet<usize>> = Vec::new();

        // @Carl go through all visits of the current train
        'visit: for (visit_i, (visit_id, Visit { locs, dwell})) in visits.iter().enumerate() {
            let mut set = HashSet::new();

            // @Carl if this is the first visit of the train it has to be in the model boundary
            let bdry = if visit_i == 0 {
                &il.boundary_routes
            } else {
                &il.boundary_out_routes
            };

            // @Carl go through all locations in the current visit
            for (loc_i, loc) in locs.iter().enumerate() {
                if let Ok(Ref::Node(pt)) = loc { // Border Node
                    set.extend(bdry.get(pt).into_iter().flat_map(move |rs| rs.iter()));
                }  else if let Ok(Ref::Object(ptA)) = loc{ // Trackside Objects (Signals, Detectors)
                    // Approach 1 (Does work, but no constraints from detectors): Objects are not to be recognized by the planner
                    continue 'visit;
                    // Approach 2 (Does not work bc signals are not nodes, after sim only detectible via Sight Events): get the point of the signal from the signal routes
                    //set.extend(il.signal_routes.get(ptA).into_iter().flat_map(move |rs| rs.iter()));
                }  else if let Ok(Ref::LineSeg(ptA, ptB)) = loc{ // LineSeg
                    set.extend(bdry.get(ptA).into_iter().flat_map(move |rs| rs.iter()));
                } else {
                    unimplemented!() // Other types of infrastructure references
                }
            }
            planner_visits.push(set);
        }
        trains.insert(*t_id, planner::input::Train {
            length: vehicle.length,
            visits: planner_visits,
        });
    }

    let mut train_ord = Vec::new();
    'constraint: for (id, ((train_a,visit_a),(train_b,visit_b), _max_time)) in &plan.order {

        // get information about the visits through their train ids and visit ids
        let (train_a_name, train_a_visits)
            = plan.trains.get(*train_a).ok_or(ConvertPlanErr::VehicleRefMissing)?;
        let (train_b_name, train_b_visits)
            = plan.trains.get(*train_b).ok_or(ConvertPlanErr::VehicleRefMissing)?;

        let visit_object_a = train_a_visits.get(*visit_a).unwrap();
        let visit_object_b = train_b_visits.get(*visit_b).unwrap();

        // Ignore visits that contain locs that are not at the boundary, the planner does not need those
        for (loc) in visit_object_a.locs.iter() {
            if let Ok(Ref::Object(ptA)) = loc { continue 'constraint; }
            else if let Ok(Ref::LineSeg(ptA, ptB)) = loc { continue 'constraint; }
        }

        for (loc) in visit_object_b.locs.iter() {
            if let Ok(Ref::Object(ptA)) = loc { continue 'constraint; }
            else if let Ok(Ref::LineSeg(ptA, ptB)) = loc { continue 'constraint; }
        }

        // TODO max_time between visits


        let visit_idx = |train_id, visit_id:&usize| {
            //println!("Finding visit for id {}", &visit_id);
            let mut visit_clone = &plan.trains.get(train_id).unwrap().1.clone();
            let mut no_intermediate_visits : ImShortGenList<Visit> = ImShortGenList::new();
            let mut count_intermediates = 0;
            for entry in visit_clone.iter() {
                for (loc_i, loc) in entry.1.locs.iter().enumerate() {
                    if let Ok(Ref::Node(pt)) = loc { // Border Node
                        no_intermediate_visits.insert(entry.1.clone());
                    }  else if let Ok(Ref::Object(ptA)) = loc{ // Trackside Objects (Signals, Detectors)
                        count_intermediates += 1;
                        continue;
                    }  else if let Ok(Ref::LineSeg(ptA, ptB)) = loc{ // LineSeg
                        no_intermediate_visits.insert(entry.1.clone());
                    } else {
                        unimplemented!() // Other types of infrastructure references
                    }
                }
            }

            // do not count intermediate visits
            let new_visit_id = if *visit_id as i32 > 0  { visit_id - count_intermediates } else {0};
            let x = no_intermediate_visits.iter().position(|(v, _)| *v == new_visit_id as usize).unwrap();
            x
        };
        // TODO unwrap crashes if visit_id is missing

        train_ord.push(planner::input::TrainOrd {
            a: (*train_a, visit_idx(*train_a, visit_a)),
            b: (*train_b, visit_idx(*train_b, visit_b)),
        });
    }

    Ok(planner::input::Usage { trains, train_ord })
}




