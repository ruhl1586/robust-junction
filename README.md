# Robust Junction

This is the repository containing the implementation of the robustness tests propsed in the bachelor thesis of Carl Schneiders. The original version of junction can be found here: http://github.com/luteberget/junction/

In order to work with the code, an adapted version of the library trainspotting is needed which is found in https://gitlab.uni-oldenburg.de/ruhl1586/robust-trainspotting/ . The contents of the altered trainspotting version need to be copied into the directory `lib/trainspotting`. 

* Download of the executables for Linux: https://cloud.uol.de/s/SocQBdR378yNNPG
* Repository of Robust Junction: https://gitlab.uni-oldenburg.de/ruhl1586/robust-junction
* Repository of Robust Trainspotting: https://gitlab.uni-oldenburg.de/ruhl1586/robust-trainspotting/




