use log::*;
use std::sync::mpsc::*;
use std::collections::HashMap;

use crate::document::model::*;
use crate::document::dgraph::*;
use crate::document::topology;
use crate::document::interlocking;
use crate::document::infview::unround_coord;

use crate::document::history;
use crate::app;
use crate::util;
use crate::util::VecMap;
use crate::document::dispatch;
use crate::document::plan;
use std::sync::Arc;
use nalgebra_glm as glm;
use crate::document::plan::SimRobustnessEvaluation;
use std::time::SystemTime;
use std::hash::Hash;
use rolling::input::staticinfrastructure::NodeId;
use crate::document::netverify;
use rolling::output::history::{History, TrainLogEvent};
use crate::document::dispatch::DispatchOutput;

pub type Generation = usize;

#[derive(Default)]
pub struct AnalysisOutput {
    pub topology: Option<(Generation, Arc<topology::Topology>)>,
    pub dgraph :Option<(Generation, Arc<DGraph>)>,
    pub interlocking :Option<(Generation, Arc<interlocking::Interlocking>)>,
    pub dispatch :Vec<Option<(Generation, dispatch::DispatchOutput)>>,
    //pub plandispatches :HashMap<usize, Vec<Option<(Generation, dispatch::DispatchOutput)>>>,
    pub plandispatches :Vec<Option<(Generation, Vec<dispatch::DispatchOutput>)>>,
}

pub struct Analysis {
    model: Undoable<Model, EditClass>,
    model_generation: Generation,
    output: AnalysisOutput,
    chan :Option<Receiver<SetData>>,
    bg :app::BackgroundJobs,
}

#[derive(Debug)]
pub enum SetData {
    DGraph(Generation, Arc<DGraph>),
    Interlocking(Generation, Arc<interlocking::Interlocking>),
    Dispatch(Generation, usize,dispatch::DispatchOutput),
    PlanDispatch(Generation, usize,Vec<dispatch::DispatchOutput>),
}

impl app::BackgroundUpdates for Analysis {
    fn check(&mut self) {
        while let Some(Ok(data)) = self.chan.as_mut().map(|r| r.try_recv()) {
            match data {
                SetData::DGraph(g, dgraph) => { self.output.dgraph = Some((g, dgraph)); },
                SetData::Interlocking(g, il) => { self.output.interlocking = Some((g, il)); },
                SetData::Dispatch(g, idx,h) => { 
                    self.output.dispatch.vecmap_insert(idx, (g, h));
                    //cache.clear_dispatch(idx);
                },
                SetData::PlanDispatch(g, plan_idx,hs) => {
                    //self.output.plandispatches.entry(plan_idx)
                        //.or_insert(Vec::new())
                        //.vecmap_insert(dispatch_idx, (g, h));
                    self.output.plandispatches.vecmap_insert(plan_idx, (g,hs));
                },
            }
        }
    }
}

impl Analysis {
    pub fn model(&self) -> &Model { &self.model.get() }
    pub fn data(&self) -> &AnalysisOutput { &self.output }
    pub fn generation(&self) -> &Generation { &self.model_generation }

    pub fn from_model(model :Model, bg: app::BackgroundJobs) -> Self {
        let mut a = Analysis {
            model: Undoable::from(model),
            model_generation: 0,
            output: Default::default(),
            chan: None,
            bg: bg,
        };
        a.update();
        a
    }

    fn update(&mut self) {
        let model = self.model.get().clone(); // persistent structs
        let gen = self.model_generation;

        // TODO @Carl Topology reduction here

        let topology = Arc::new(topology::convert(&model, 50.0).unwrap());
        self.output.topology = Some((gen,topology.clone()));

        let (tx,rx) = channel();
        self.chan = Some(rx);

        let dummy_visits : &HashMap<usize, HashMap<NodeId, f64>> =  &HashMap::new();

        self.bg.execute(move || {
            info!("Background thread starting");
            let mut model = model;  // move model into thread
            let tx = tx;        // move sender into thread

            //let dgraph = dgraph::calc(&model); // calc dgraph from model.
            let dgraph = DGraphBuilder::convert(&topology).expect("dgraph conversion failed");
            let dgraph = Arc::new(dgraph);

            info!("Dgraph successful with {:?} nodes", dgraph.rolling_inf.nodes.len());

            let send_ok = tx.send(SetData::DGraph(gen, dgraph.clone()));
            if !send_ok.is_ok() { println!("job canceled after dgraph"); return; }
            // if tx fails (channel is closed), we don't need 
            // to proceed to next step. Also, there is no harm
            // in *trying* to send the data from an obsolete thread,
            // because the update function will have replaced its 
            // receiver end of the channel, so it will anyway not
            // be placed into the struct.

            let interlocking = interlocking::calc(&dgraph); 
            let interlocking = Arc::new(interlocking);
                // calc interlocking from dgraph
            let send_ok = tx.send(SetData::Interlocking(gen, interlocking.clone()));
            if !send_ok.is_ok() { println!("job canceled after interlocking"); return; }
            info!("Interlocking successful with {:?} routes", interlocking.routes.len());


            // for (i,dispatch) in model.dispatches.iter() {
            //     //let history = dispatch::run(&dgraph, &interlocking, &dispatch);
            //     let (history,route_refs) = history::get_history(model.vehicles.data(),
            //                                        &dgraph.rolling_inf,
            //                                        &interlocking,
            //                                        &(dispatch.commands),
            //                                 &HashMap::new(),
            //                                     false,
            //                                         &dummy_visits).unwrap(); //TODO @Carl hand over real locatios
            //     info!("Simulation successful {:?}", &dispatch.commands);
            //     let view = dispatch::DispatchOutput::from_history(dispatch.clone(), &dgraph, history, SimRobustnessEvaluation::default());
            //     let send_ok = tx.send(SetData::Dispatch(gen, *i, view));
            //     if !send_ok.is_ok() { println!("job canceled after dispatch"); return; }
            // }

            for (plan_idx,plan) in model.plans.iter() {
                println!("\n==================== {}: ============================\n", plan.name);
                let now = SystemTime::now();


                let planresults = plan::get_dispatches(&dgraph, &interlocking,
                                             model.vehicles.data(),
                                             plan).unwrap();
                // measure planning time
                match now.elapsed() {
                    Ok(elapsed) => {
                        // it prints '2'
                        println!("Planning for {} took {} milliseconds", plan.name, elapsed.as_millis());
                    }
                    Err(e) => {
                        // an error occurred!
                        println!("Error measuring planning time: {:?}", e);
                    }
                }

                info!("Planning successful. {:?}", planresults);

                let dispatches = planresults.into_iter().map(|(d,h, e, o)| {
                    dispatch::DispatchOutput::from_history(d, &dgraph, h, e, o)
                }).collect();

                // read edges from graph
                // collect all possible paths from all dispatch historys (see plan.rs)
                // build & solve milp
                let net_verification = true;
                if net_verification {
                    let handler = netverify::NetVerifierHandler::new(&dgraph, &dispatches, );
                    println!("TTQs in optimal case:");
                    for (di, dispatch) in dispatches.iter().enumerate() {
                        println!("Dispatch {}: ", di);
                        for (constraint_id, ttq) in dispatch.optimal_ttqs.clone() {
                            println!("Constraint {} has TTQ {}", constraint_id, ttq);
                        }
                    }
                    println!("");

                    println!("Obstruction Classification:");
                    let before_brute = SystemTime::now();
                    let (brute_intolerable, brute_tolerable) = handler.search_all_obstructions(plan.number_of_interruptions);
                    // measure brute force analysis time
                    match before_brute.elapsed() {
                        Ok(elapsed) => {
                            // it prints '2'
                            println!("Obstruction classification took {} milliseconds", elapsed.as_millis());
                        }
                        Err(e) => {
                            // an error occurred!
                            println!("Error measuring obstruction Classification time: {:?}", e);
                        }
                    }

                    println!("Tolerable Obstructions: ");
                    for obstruction in brute_tolerable {
                        println!("{:?}", obstruction);
                    }
                    println!("Intolerable Obstructions: ");
                    for obstruction in brute_intolerable {
                        println!("{:?}", obstruction);
                    }


                    println!();

                    let search_redundancies = false;

                    if search_redundancies {
                        println!("\nRedundancy analysis:");
                        let before_redundancy = SystemTime::now();
                        let redundancies = handler.search_all_redundancies(0.0);
                        // measure brute force analysis time
                        match before_redundancy.elapsed() {
                            Ok(elapsed) => {
                                // it prints '2'
                                println!("Redundancy search took {} milliseconds", elapsed.as_millis());
                            }
                            Err(e) => {
                                // an error occurred!
                                println!("Error measuring Redundancy search time: {:?}", e);
                            }
                        }
                        println!("Redundant Tracks: ");
                        for redundancy in redundancies {
                            println!("{:?}", redundancy);
                        }
                    }
                }



                let send_ok = tx.send(SetData::PlanDispatch(gen, *plan_idx, dispatches));
                if !send_ok.is_ok() { println!("job cancelled after plan dispatch {}", plan_idx); }
            }

        });
    }



    pub fn edit_model(&mut self, mut f :impl FnOnce(&mut Model) -> Option<EditClass>) {
        let mut new_model = self.model.get().clone();
        let cl = f(&mut new_model);
        self.set_model(new_model, cl);
    }

    pub fn set_model(&mut self, m :Model, cl :Option<EditClass>) {
        info!("Updating model");
        self.model.set(m, cl);
        self.on_changed();
    }

    pub fn override_edit_class(&mut self, cl :EditClass) {
        self.model.override_edit_class(cl);
    }

    pub fn undo(&mut self) { if self.model.undo() { self.on_changed(); } }
    pub fn redo(&mut self) { if self.model.redo() { self.on_changed(); } }
    pub fn can_undo(&self) -> bool { self.model.can_undo() }
    pub fn can_redo(&self) -> bool { self.model.can_redo() }

    fn on_changed(&mut self) {
        // TODO
        // kself.fileinfo.set_unsaved();
        self.model_generation += 1;
        self.update();
    }


    pub fn get_rect(&self, a :PtC, b :PtC) -> Vec<Ref> {
        let mut r = Vec::new();
        for (a,b) in self.model().get_linesegs_in_rect(a,b) {
            r.push(Ref::LineSeg(a,b));
        }
        if let Some((_,topo)) = self.data().topology.as_ref() {
            for (pt,_) in topo.locations.iter() {
                if util::in_rect(glm::vec2(pt.x as f32,pt.y as f32), a,b) {
                    r.push(Ref::Node(*pt));
                }
            }
        }
        for (pta,_) in self.model().objects.iter() {
            if util::in_rect(unround_coord(*pta), a, b) {
                r.push(Ref::Object(*pta));
            }
        }
        r
    }

    pub fn get_closest(&self, pt :PtC) -> Option<(Ref,f32)> {
        let (mut thing, mut dist_sqr) = (None, std::f32::INFINITY);
        if let Some(((p1,p2),_param,(d,_n))) = self.model().get_closest_lineseg(pt) {
            thing = Some(Ref::LineSeg(p1,p2));
            dist_sqr = d;
        }

        if let Some((p,d)) = self.get_closest_node(pt) {
            if d < 0.5*0.5 {
                thing = Some(Ref::Node(p));
                dist_sqr = d;
            }
        }

        if let Some(((p,_obj),d)) = self.model().get_closest_object(pt) {
            if d < 0.5*0.5 {
                thing = Some(Ref::Object(*p));
                dist_sqr = d;
            }
        }

        thing.map(|t| (t,dist_sqr))
    }

    pub fn get_closest_node(&self, pt :PtC) -> Option<(Pt,f32)> {
        let (mut thing, mut dist_sqr) = (None, std::f32::INFINITY);
        for p in corners(pt) {
            for (px,_) in self.data().topology.as_ref()?.1.locations.iter() {
                if &p == px {
                    let d = glm::length2(&(pt-glm::vec2(p.x as f32,p.y as f32)));
                    if d < dist_sqr {
                        thing = Some(p);
                        dist_sqr = d;
                    }
                }
            }
        }
        thing.map(|t| (t,dist_sqr))
    }

    // pub fn discraded_reduce_model() {
    //     // identify coordinates of the interruptions
    //     let mut tracks_to_remove : Vec<(Pt, Pt)> = vec![];
    //     for (i, j) in interrupted_tracks {
    //         let mut i_j_path: Vec<(NodeId, NodeId)> = vec![];
    //         for path in &dgraph.all_paths.1 {
    //             let mut previous_node_available = false;
    //             let mut previous_node: NodeId = 0;
    //             let mut recording = false;
    //             for edge in path {
    //                 if (edge.0 == i) {
    //                     recording = true;
    //                     i_j_path.push((edge.0, edge.1));
    //                     previous_node = edge.1;
    //                     previous_node_available = true;
    //                     continue;
    //                 }
    //                 if edge.0 == j {
    //                     recording = false;
    //                     break;
    //                 }
    //                 if recording {
    //                     if previous_node_available {
    //                         i_j_path.push((previous_node, edge.0))
    //                     }
    //                     i_j_path.push((edge.0, edge.1));
    //                     previous_node = edge.1;
    //                 }
    //             }
    //             if !i_j_path.is_empty() { break }; // stop if one path was found
    //         }
    //         println!("Path from {} to {}: {:?}", i, j, i_j_path);
    //
    //         // removing the first segment is enough
    //         for(i, j) in i_j_path {
    //             // Manual Case definition because I couldnt figure out the type
    //             // problems. I am so sorry.
    //             if let Some(coordinates_i) = dgraph.detector_ids.get_by_left(&i) {
    //                 println!("{} is detector", i);
    //                 if let Some(coordinates_j) = dgraph.detector_ids.get_by_left(&j) {
    //                     println!("{} is detector", j);
    //                     tracks_to_remove.push((*coordinates_i, *coordinates_j));
    //                     tracks_to_remove.push((*coordinates_j, *coordinates_i));
    //                 } else if let Some(coordinates_j) = dgraph.switch_ids.get_by_left(&j) {
    //                     println!("{} is switch", j);
    //                     tracks_to_remove.push((*coordinates_i, *coordinates_j));
    //                     tracks_to_remove.push((*coordinates_j, *coordinates_i));
    //                 } else if let Some(coordinates_j) = dgraph.node_ids.get_by_left(&j) {
    //                     println!("{} is node", j);
    //                     tracks_to_remove.push((*coordinates_i, *coordinates_j));
    //                     tracks_to_remove.push((*coordinates_j, *coordinates_i));
    //                 }
    //             } else if let Some(coordinates_i) = dgraph.switch_ids.get_by_left(&i) {
    //                 println!("{} is switch", i);
    //                 if let Some(coordinates_j) = dgraph.detector_ids.get_by_left(&j) {
    //                     tracks_to_remove.push((*coordinates_i, *coordinates_j));
    //                     tracks_to_remove.push((*coordinates_j, *coordinates_i));
    //                 } else if let Some(coordinates_j) = dgraph.switch_ids.get_by_left(&j) {
    //                     tracks_to_remove.push((*coordinates_i, *coordinates_j));
    //                     tracks_to_remove.push((*coordinates_j, *coordinates_i));
    //                 } else if let Some(coordinates_j) = dgraph.node_ids.get_by_left(&j) {
    //                     tracks_to_remove.push((*coordinates_i, *coordinates_j));
    //                     tracks_to_remove.push((*coordinates_j, *coordinates_i));
    //                 }
    //             } else if let Some(coordinates_i) = dgraph.node_ids.get_by_left(&i) {
    //                 println!("{} is node", i);
    //                 if let Some(coordinates_j) = dgraph.detector_ids.get_by_left(&j) {
    //                     tracks_to_remove.push((*coordinates_i, *coordinates_j));
    //                 } else if let Some(coordinates_j) = dgraph.switch_ids.get_by_left(&j) {
    //                     tracks_to_remove.push((*coordinates_i, *coordinates_j));
    //                 } else if let Some(coordinates_j) = dgraph.node_ids.get_by_left(&j) {
    //                     tracks_to_remove.push((*coordinates_i, *coordinates_j));
    //                 }
    //             }
    //         }
    //     }
    //
    //     println!("Tracks to remove: {:?}", tracks_to_remove);
    //
    //     // remove tracks from graph
    //     let mut reduced_model = model.clone();
    //     println!("Linesegs in model: {:?}", reduced_model.linesegs);
    //     for ((i, j)) in tracks_to_remove {
    //         reduced_model.delete(Ref::LineSeg(i, j));
    //     }
    //     println!("Linesegs in model after deletion: {:?}", reduced_model.linesegs);
    //     let reduced_topology = Arc::new(topology::convert(&reduced_model, 50.0).unwrap());
    //     let reduced_dgraph = DGraphBuilder::convert(&reduced_topology).expect("dgraph conversion failed");
    //     let reduced_dgraph = Arc::new(reduced_dgraph);
    //     let reduced_interlocking = interlocking::calc(&reduced_dgraph);
    //     let reduced_interlocking = Arc::new(reduced_interlocking);
    //
    //     // plan & simulate with reduced graph
    //     let new_planresults = plan::get_dispatches(&reduced_dgraph, &reduced_interlocking,
    //                                                model.vehicles.data(),
    //                                                plan).unwrap();
    //
    //     // TODO evaluate resulting plan(s)
    //     println!("Planning reduced graph successful. {:?}", new_planresults);
    // }

}

