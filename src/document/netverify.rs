extern crate lp_modeler;
extern crate combinations;
extern crate coin_cbc;
extern crate coin_cbc_sys;

use std::collections::{HashMap, HashSet};
use lp_modeler::solvers::{SolverTrait, CbcSolver, GurobiSolver};
use lp_modeler::dsl::*;
use lp_modeler::format::lp_format::LpFileFormat;
use std::hash::Hash;
use combinations::Combinations;
use self::lp_modeler::solvers::Status;
use coin_cbc::{raw::Status as CBCStatus, Model, Sense};
use std::ffi::CStr;
use crate::document::dgraph::{DGraph, edge_length};
use rolling::input::staticinfrastructure::{NodeId, Edges};
use crate::document::history::History;
use crate::document::model::PlanSpec;
use bimap::BiMap;
use crate::document::dispatch::DispatchOutput;
use rolling::output::history::TrainLogEvent;


// TODO @Carl coinor-libcbc-dev ins README, CBC solver (Dependencies)
pub struct NetVerifierHandler {
    //verifier: NetVerifier,
    node_list : Vec<usize>,
    merged_nodes_map: BiMap<usize, (NodeId, NodeId)>,
    partner_nodes: HashMap<NodeId, NodeId>,
    trains: HashMap<usize, (NodeId, NodeId)>,
    train_routes: HashMap<usize, Vec<Vec<(usize, usize)>>>,
    edges: Vec<(usize, usize)>,
    costs: HashMap<(NodeId, NodeId), f32>,
    x: HashMap<usize, HashMap<(usize, usize), usize>>
}

pub type TolerableObstructionScenario  = Vec<(usize, usize)>;
pub type IntolerableObstructionScenario = Vec<(usize, usize)>;


impl NetVerifierHandler {

    /// Analyse all obstruction scenarios
    /// Input: A maximal number of simultaneously obstructed tracks
    /// Output:
    /// - a list of intolerable obstruction scenarios
    /// - a list of tolerable obstruction scenarios with their cost
    pub fn search_all_obstructions(&self, number_of_interrupts: usize) -> (Vec<IntolerableObstructionScenario>, Vec<TolerableObstructionScenario>) {
        let verifier = NetVerifier::new(
            self.node_list.clone(),
            self.edges.clone(),
            self.costs.clone(),
            self.x.clone(),
            self.trains.clone(),
            number_of_interrupts);

        verifier.analyze_all_obstructions()
    }

    /// Analyse all obstruction scenarios by going from largest first
    /// Input: A maximal number of simultaneously obstructed tracks
    /// Output:
    /// - a list of intolerable obstruction scenarios
    /// - a list of tolerable obstruction scenarios with their cost
    pub fn search_all_obstructions_reverse(&self) -> (Vec<IntolerableObstructionScenario>, Vec<TolerableObstructionScenario>) {
        let verifier = NetVerifier::new(
            self.node_list.clone(),
            self.edges.clone(),
            self.costs.clone(),
            self.x.clone(),
            self.trains.clone(),
            1);

        verifier.analyze_all_obstructions_reverse()
    }

    pub fn search_all_redundancies(&self, error: f32) -> Vec<TolerableObstructionScenario> {
        let verifier = NetVerifier::new(
            self.node_list.clone(),
            self.edges.clone(),
            self.costs.clone(),
            self.x.clone(),
            self.trains.clone(),
            1);

        verifier.get_redundant_tracks(error)
    }

    /// Convert Information from infrastructure graph and dispatch plans into a form suitable
    /// for building an MIP, including data structures to reverse convert MIP output into
    /// information usable with the infrastructure graph
    pub fn new(dgraph: &DGraph, dispatches:&Vec<DispatchOutput>) -> NetVerifierHandler{
        //let nodeMap = map_nodes_from_dgraph(dgraph);
        let mut node_list : HashSet<NodeId> = HashSet::new();
        let mut trains: HashMap<usize, (NodeId, NodeId)> = HashMap::new();

        let mut train_routes: HashMap<usize, Vec<Vec<NodeId>>> = HashMap::new();
        let mut train_dispatches : Vec<(usize, usize, Vec<NodeId>)> = vec![]; // Dispatch ID, Train ID, Nodes

        //init train routes
        for (train_id, _) in dispatches.get(0).unwrap().history.trains.iter().enumerate() {
            train_routes.insert(train_id, vec![]);
        }

        // Get all train OD-Pairs and routes, uses node ids from double noded graph
        for (di, dispatch) in dispatches.iter().enumerate() {
            let history: &History = &dispatch.history;
            // Durchsuche alle events jedes Zugs
            for (train_id, (train_name, params, events)) in history.trains.iter().enumerate() {
                let mut event_counter = 0;
                let mut train_nodes: Vec<NodeId> = vec![];
                for event in events {
                    match event {
                        // Wenn Event relevant, also Detector, Einfahrt/Ausfahrt:
                        // Füge entsprechenden Node der Liste aller Nodes hinzu
                        // Füge entsprechenden Node den train_nodes hinzu
                        // Switches werden nicht beachtet, da sie je nach Einfahrtrichtung als
                        // Node gezählt werden oder nicht
                        TrainLogEvent::Node(node_id) => {
                            if event_counter == 0 {
                                train_nodes.push(*node_id);
                                node_list.insert(*node_id);
                                event_counter += 1;
                            } else if let Some(relevant_id) = dgraph.detector_ids.get_by_left(node_id) {
                                train_nodes.push(*node_id);
                                node_list.insert(*node_id);
                                event_counter += 1;
                            // } else if let Some(relevant_id) = dgraph.switch_ids.get_by_left(node_id) {
                            //     train_nodes.push(*node_id);
                            //     node_list.insert(*node_id);
                            //     event_counter += 1
                            } else if let Some(node) = dgraph.rolling_inf.nodes.get(*node_id) {
                                match node.edges {
                                    Edges::ModelBoundary => {
                                        train_nodes.push(*node_id);
                                        node_list.insert(*node_id);
                                        event_counter += 1
                                    },
                                    _ => {}
                                };
                            }
                        },
                        _ => {}
                    }
                }
                // insert current route to list of all train dispatches for visual identification of routes
                train_dispatches.push((di, train_id, train_nodes.clone() ));

                // insert current route to set of routes for this train
                let mut all_routes = train_routes.get(&train_id).unwrap().clone();
                all_routes.push(train_nodes.clone());
                train_routes.insert(train_id, all_routes);
            }
        }

        // remove duplicate routes for each train
        for (train_id, routes) in &mut train_routes {
            routes.sort();
            routes.dedup();
        }

        let mut merged_nodes_map: BiMap<usize, (NodeId, NodeId)> = BiMap::new();
        let mut partner_nodes: HashMap<NodeId, NodeId> = HashMap::new();
        let mut node_counter: usize = 0;

        // for each node find partner node and store
        // in partner nodes map for quick referencing
        // in merged nodes map for assigning pairs to single ids and back
        for node in &node_list {
            let partner_node = dgraph.rolling_inf.nodes.get(*node).unwrap().other_node.clone();
            let pair = get_ordered_node_pair(*node, partner_node);

            // insert if node pair is unknown
            if !merged_nodes_map.contains_right(&pair) {
                merged_nodes_map.insert(node_counter, pair);
                partner_nodes.insert(pair.0, pair.1);
                partner_nodes.insert(pair.1, pair.0);
                node_counter += 1;
            }
        }

        let mut train_routes_single_node: HashMap<usize, Vec<Vec<(usize, usize)>>> = HashMap::new();
        let mut costs: HashMap<(NodeId, NodeId), f32> = HashMap::new();

        // convert train routes into single node representation with node pairs
        for (train, routes) in &train_routes {
            let mut single_node_routes : Vec<Vec<(usize, usize)>> = vec![];
            for route in routes {
                let mut single_node_route: Vec<(usize, usize)> = vec![];
                for i in 0..route.len() {
                    if i == route.len() - 1 {break}
                    let current_node = route.get(i).unwrap();
                    let next_node = route.get(i+1).unwrap();
                    let current_node_partner = partner_nodes.get(current_node).unwrap();
                    let next_node_partner = partner_nodes.get(next_node).unwrap();

                    let current_pair = get_ordered_node_pair(*current_node, *current_node_partner);
                    let next_pair = get_ordered_node_pair(*next_node, *next_node_partner);

                    let current_node_single = merged_nodes_map.get_by_right(&current_pair).unwrap();
                    let next_node_single = merged_nodes_map.get_by_right(&next_pair).unwrap();

                    single_node_route.push((*current_node_single, *next_node_single));

                    if !costs.contains_key(&(*current_node_single, *next_node_single)) {
                        //let mileage_current = *dgraph.mileage.get(current_node).unwrap() as f32;
                        //let mileage_next = *dgraph.mileage.get(next_node).unwrap() as f32;
                        //costs.insert((*current_node_single, *next_node_single), f32::abs(mileage_next - mileage_current));
                        costs.insert((*current_node_single, *next_node_single), 1.0);
                    }
                }
                single_node_routes.push(single_node_route);
            }
            // insert O/D-pair
            trains.insert(*train, (single_node_routes.first().unwrap().first().unwrap().0,
                                     single_node_routes.last().unwrap().last().unwrap().1));

            train_routes_single_node.insert(*train, single_node_routes);
        }

        // read edges from routes
        let mut edges: Vec<(usize, usize)> = vec![];
        for (train_id, routes) in &train_routes_single_node {
            for route in routes {
                for (i, j) in route {
                    if !edges.contains(&(*i, *j)) {
                        edges.push((*i, *j));
                    }

                    // double edges that are not doubled: edges can exist in both directions
                    if !edges.contains(&(*j, *i)) {
                        edges.push((*j, *i));
                        let cost = costs.get(&(*i, *j)).unwrap();
                        costs.insert((*j, *i), *cost);
                    }
                }
            }
        }

        // get list of merged (single) nodes:
        let merged_nodes : Vec<usize> = merged_nodes_map.left_values().map(|x| *x ).collect();

        // convert edges to binary represenation
        let mut x : HashMap<usize, HashMap<(usize, usize), usize>> = HashMap::new();
        for (train, _) in &trains {
            let mut current_x: HashMap<(usize, usize), usize>  = HashMap::new();

            // init all paths with zeros
            for (i, j) in &edges {
                current_x.insert((*i, *j), 0);
            }

            // replace with 1s where the train can drive
            for route in train_routes_single_node.get(&train).unwrap() {
                for (i, j) in &edges {
                    if route.contains(&(*i, *j)) {
                        current_x.insert((*i, *j), 1);
                    }
                }
            }
            x.insert(*train, current_x);
        }

        // Print Dispatch Plans & Routes in single node represenation for visually matching
        // obstructions
        for (di, tid, nodes) in train_dispatches {

            let mut single_nodes : Vec<usize> = vec![];
            for node in nodes {
                let pair = get_ordered_node_pair(node, *partner_nodes.get(&node).unwrap());
                let single_node = merged_nodes_map.get_by_right(&pair).unwrap();
                single_nodes.push(*single_node);
            }
            println!("Dispatch {}, Train {}, Route: {:?}", di, tid, single_nodes);
        }



        //println!("All relevant nodes: {:?}", node_list);
        // println!("All Single Nodes: {:?}", merged_nodes);
        // println!("All node merges: {:?}", merged_nodes_map);
        // println!("All relevant edges between merged nodes: {:?}", &edges);
        //println!("Costs between all edges: {:?}", costs);
        // println!("All Train OD Pairs for merged Nodes: {:?}", trains);
        // println!("Values for x: {:?}", x);
        // // for (train_id, routes) in &train_routes {
        // //     println!("Routes for train {}: ", train_id);
        // //     for route in routes {
        // //         println!("{:?}", route);
        // //     }
        // // }
        // for (train_id, routes) in &train_routes_single_node {
        //     println!("Single node routes for train {}: ", train_id);
        //     for route in routes {
        //         println!("{:?}", route);
        //     }
        // }




        NetVerifierHandler {
            node_list: merged_nodes,
            merged_nodes_map,
            partner_nodes,
            trains,
            train_routes: train_routes_single_node,
            edges,
            costs,
            x
        }
    }

}

fn get_ordered_node_pair(a: NodeId, b: NodeId) -> (NodeId, NodeId){
    if a < b {
        (a, b)
    } else {
        (b, a)
    }
}




struct NetVerifier {
    nodes: Vec<usize>,
    edges: Vec<(usize, usize)>,
    cost: HashMap<(usize, usize), f32>,
    x : HashMap<usize, HashMap<(usize, usize), usize>>,
    trains: HashMap<usize, (usize, usize)>,
    n: usize
}

impl NetVerifier {
    pub fn new(nodes: Vec<usize>,
           edges: Vec<(usize, usize)>,
           cost: HashMap<(usize, usize), f32>,
           x : HashMap<usize, HashMap<(usize, usize), usize>>,
           trains: HashMap<usize, (usize, usize)>,
           n: usize) -> NetVerifier {

        NetVerifier{
            nodes,
            edges,
            cost,
            x,
            trains,
            n
        }
    }

    fn all_combinations_of_n_obstructions(&self, n: usize) -> Vec<HashMap<(usize, usize), i32>>{
        let mut result : Vec<HashMap<(usize, usize), i32>> = vec![];

        let mut bidirectional_edges : Vec<(usize, usize)> = vec![];
        for (i, j) in self.edges.clone() {
            if !bidirectional_edges.contains(&(j, i)) {
                bidirectional_edges.push((i, j));
            }
        }

        let mut combinations = vec![];

        if n < bidirectional_edges.len() {
            combinations = Combinations::new(bidirectional_edges.clone(), n).collect();
        } else {
            combinations = vec![bidirectional_edges.clone()];
        }

        for combination in combinations {
            let mut map : HashMap<(usize, usize), i32> = HashMap::new();
            for (i, j) in &bidirectional_edges {
                if combination.contains(&(*i, *j)) {
                    map.insert((*i, *j), 1);
                    map.insert((*j, *i), 1);
                } else {
                    map.insert((*i, *j), 0);
                    map.insert((*j, *i), 0);
                }
            }
            result.push(map);
        }

        result
    }

    fn all_obstructions(&self) -> Vec<HashMap<(usize, usize), i32>>{
        let mut result : Vec<HashMap<(usize, usize), i32>> = vec![];

        let mut bidirectional_edges : Vec<(usize, usize)> = vec![];
        for (i, j) in self.edges.clone() {
            if !bidirectional_edges.contains(&(j, i)) {
                bidirectional_edges.push((i, j));
            }
        }

        for n in 1..bidirectional_edges.len() {
            let combinations: Vec<_> = Combinations::new(bidirectional_edges.clone(), n).collect();

            for combination in combinations {
                let mut map: HashMap<(usize, usize), i32> = HashMap::new();
                for (i, j) in &bidirectional_edges {
                    if combination.contains(&(*i, *j)) {
                        map.insert((*i, *j), 1);
                        map.insert((*j, *i), 1);
                    } else {
                        map.insert((*i, *j), 0);
                        map.insert((*j, *i), 0);
                    }
                }
                result.push(map);
            }
        }

        result
    }

    fn no_obstructions(&self,) -> HashMap<(usize, usize), i32>{
        let mut result : HashMap<(usize, usize), i32> = HashMap::new();

        let mut bidirectional_edges : Vec<(usize, usize)> = vec![];
        for (i, j) in self.edges.clone() {
            if !bidirectional_edges.contains(&(j, i)) {
                bidirectional_edges.push((i, j));
            }
        }

        for (i, j) in &bidirectional_edges {
            result.insert((*i, *j), 0);
            result.insert((*j, *i), 0);
        }

        result
    }



    /// Sort all scenarios into tolerable and intolerable
    pub fn analyze_all_obstructions(&self) -> (Vec<IntolerableObstructionScenario>, Vec<TolerableObstructionScenario>){
        let mut combinations_to_search: Vec<Vec<HashMap<(usize, usize), i32>>> = vec![];
        let mut intolerable_obstructions: Vec<Vec<(usize, usize)>> = vec![];
        let mut tolerable_obstructions : Vec<Vec<(usize, usize)>> = vec![];

        for i in 1..&self.n+1 {
            combinations_to_search.push(self.all_combinations_of_n_obstructions(i));
        }

        //println!("All Obstruction Scenarios: {:?}\n", combinations_to_search);

        for i in 0..self.n {
            for y in &mut combinations_to_search.get(i).unwrap().clone() {
                let solution = self.build_and_solve(&y);
                match solution {
                    Ok((problem, status, result, value)) => {
                        if status == Status::Optimal {
                            //println!("Scenario {:?} is optimal", scenario_to_obstruction_list(y.clone()));
                            tolerable_obstructions.push(scenario_to_obstruction_list(y.clone()));
                        } else {
                            //println!("Scenario {:?} takes else branch with state {:?}", scenario_to_obstruction_list(y.clone()), status);
                            intolerable_obstructions.push(scenario_to_obstruction_list(y.clone()));
                            for j in i+1..self.n {
                                let mut y_index = 0;
                                loop {
                                    if y_index >= combinations_to_search.get(j).unwrap().len() { break; }
                                    let y_candidate = combinations_to_search.get(j).unwrap().get(y_index).unwrap().clone();
                                    if is_in_superset(&y, &y_candidate) {
                                        intolerable_obstructions.push(scenario_to_obstruction_list(y_candidate.clone()));
                                        combinations_to_search.get_mut(j).unwrap().remove(y_index);
                                    }
                                    y_index += 1;
                                }
                            }
                        }

                    },
                    Err(msg) => {
                        //println!("Scenario {:?} takes Err branch", scenario_to_obstruction_list(y.clone()));
                        intolerable_obstructions.push(scenario_to_obstruction_list(y.clone()));
                        for j in i+1..self.n {
                            let mut y_index = 0;
                            loop {
                                let y_candidate = combinations_to_search.get(j).unwrap().get(y_index).unwrap().clone();
                                if is_in_superset(&y, &y_candidate) {
                                    println!("Deduced Intolerable Scenario: {:?} at index {} in list {} of length {}", y_candidate, y_index, j, combinations_to_search.get(j).unwrap().clone().len());
                                    intolerable_obstructions.push(scenario_to_obstruction_list(y_candidate.clone()));
                                    combinations_to_search.get_mut(j).unwrap().remove(y_index);
                                }
                                y_index += 1;
                                if y_index >= combinations_to_search.get(j).unwrap().len() { break; }
                            }
                        }
                    }
                }
            }
        }

        (intolerable_obstructions, tolerable_obstructions)

    }

    /// Sort all scenarios into tolerable and intolerable
    pub fn analyze_all_obstructions_reverse(&self) -> (Vec<IntolerableObstructionScenario>, Vec<TolerableObstructionScenario>){
        let mut intolerable_obstructions: Vec<Vec<(usize, usize)>> = vec![];
        let mut tolerable_obstructions : Vec<Vec<(usize, usize)>> = vec![];
        let mut explored_scenarios : Vec<Vec<(usize, usize)>> = vec![];


        loop {
            let solution = self.build_and_solve_largest_obstruction(&explored_scenarios);
            match solution {
                Ok((problem, status, scenario)) => {
                    if status == Status::Optimal {
                        let obstruction_list = scenario_to_obstruction_list(scenario.clone());
                        if obstruction_list.is_empty() {break;} // If list is empty, then no smaller obstructions were found

                        for mut subset in power_set(&obstruction_list) {
                            let mut sorted_subset = subset.clone();
                            sorted_subset.retain(|&(i, j)| subset.contains(&(j, i)) );
                            if !sorted_subset.is_empty() {
                                explored_scenarios.push(sorted_subset.clone());
                                tolerable_obstructions.push(sorted_subset.clone());
                            }

                        }
                        explored_scenarios.sort();
                        tolerable_obstructions.sort();
                        explored_scenarios.dedup();
                        tolerable_obstructions.dedup();

                    } else {
                        println!("Error building & solving, in else case");
                        break;
                    }
                },
                Err(msg) => {
                    println!("Error building & solving: {}", msg);
                    break;
                }
            }
        }

        // get all obstructions
        let all_obstruction_scenarios = self.all_obstructions();
        let mut all_obstruction_list: Vec<Vec<(usize, usize)>> = vec![];
        for scenario in all_obstruction_scenarios {
            all_obstruction_list.push(scenario_to_obstruction_list(scenario));
        }

        // sort all obstructions
        for obstruction in &mut all_obstruction_list {
            obstruction.sort();
        }

        // sort all tolerable obstructions
        for obstruction in &mut tolerable_obstructions {
            obstruction.sort();
        }

        // build intolerable set = all \ tolerable:
        for obstruction in all_obstruction_list {
            if !tolerable_obstructions.contains(&obstruction) {
                intolerable_obstructions.push(obstruction);
            }
        }

        (intolerable_obstructions, tolerable_obstructions)

    }

    pub fn get_redundant_tracks(&self, error: f32) -> Vec<TolerableObstructionScenario> {
        let mut redundant_tracks : Vec<TolerableObstructionScenario> = vec![];
        let optimal_scenario = self.no_obstructions();
        let mut optimal_cost = 0.0;

        // Get optimal cost
        let solution = self.build_and_solve(&optimal_scenario);
        match solution {
            Ok((problem, status, res, value)) => {
                optimal_cost = value + error;
                //println!("Optimal cost: {}", &optimal_cost);
            },
            Err(msg) => {
                return redundant_tracks
            }
        }

        // get largest obstruction with no cost reduction
        loop {
            let solution = self.build_and_solve_redundancy(&redundant_tracks, optimal_cost);
            match solution {
                Ok((problem, status, scenario)) => {
                    if status == Status::Optimal {
                        let obstruction_list = scenario_to_obstruction_list(scenario.clone());
                        if obstruction_list.is_empty() {
                            break;
                        } // If list is empty, then no smaller obstructions were found
                        redundant_tracks.push(obstruction_list.clone());
                    } else {
                        println!("Error building & solving, in else case");
                        break;
                    }
                },
                Err(msg) => {
                    println!("Error building & solving: {}", msg);
                    break;
                }
            }
        }

        redundant_tracks

    }

    fn build_and_solve_redundancy(&self, forbidden: &Vec<Vec<(usize, usize)>>, optimal_value: f32) -> Result<(String, Status, HashMap<(usize, usize), i32>), String> {
        let mut problem = LpProblem::new("Find Largest Obstruction", LpObjective::Maximize);

        // Build a_ij^t Variables
        let mut aijt = HashMap::new();
        for t in 0..self.trains.len() {
            for (i, j) in &self.edges {
                aijt.insert((i, j, t), LpBinary::new(&format!("a_{}_{}_{}", i, j, t)));
            }
        }

        // Build y_ij variables
        let mut yij = HashMap::new();
        for (i, j) in &self.edges {
            yij.insert((i, j), LpBinary::new(&format!("y_{}_{}", i, j)));
        }

        // build n
        let n = LpInteger::new("n");

        // Objective function
        problem += (1*&n);

        let mut obj_vec: Vec<LpExpression> = Vec::new();
        for (&(&i, &j), y) in &yij{
            obj_vec.push(1 * y);
        }
        problem += lp_sum(&obj_vec).equal(2*&n);

        // Flow conservation
        for (t, _) in &self.trains {
            for i in &self.nodes {
                let mut sum_1_vec: Vec<LpExpression> = Vec::new();
                let mut sum_2_vec: Vec<LpExpression> = Vec::new();
                for j in &self.nodes {
                    if self.edges.contains(&(*i, *j)){
                        sum_1_vec.push(1*aijt.get(&(i, j, *t)).unwrap());
                        sum_2_vec.push(1*aijt.get(&(j, i, *t)).unwrap());
                    }
                }

                let target: i32 =
                    if *i == self.trains.get(&t).unwrap().0 {
                        1
                    } else if *i == self.trains.get(&t).unwrap().1 {
                        -1
                    } else {
                        0
                    };

                if !&sum_1_vec.is_empty() {
                    problem += (lp_sum(&sum_1_vec) - lp_sum(&sum_2_vec)).equal(target)
                } else {
                    //println!("No Edges found for Node {}", i);
                }

            }
        }

        // Blocked Tracks are not used:
        for t in 0..self.trains.len() {
            for (i,j) in &self.edges {
                problem += (1*aijt.get(&(i, j, t)).unwrap()).le(1-1*yij.get(&(i, j)).unwrap())
            }
        }

        // Tracks are blocked both ways
        for (i, j) in &self.edges {
            problem += (1*yij.get(&(i, j)).unwrap()).equal(1*yij.get(&(j, i)).unwrap());
        }

        // a's follow the pre-defined routes
        for t in 0..self.trains.len() {
            for (i, j) in &self.edges {
                let x_target = *self.x.get(&t).unwrap().get(&(*i, *j)).unwrap() as i32;
                // let mut x_sum = 0;
                // for route in self.x.get(&t).unwrap().iter() {
                //     x_sum += *route.get(&(*i, *j)).unwrap();
                //     //println!("Train {}, Route {}, Edge {}-{} has value {}", t, )
                // }
                problem += (aijt.get(&(i, j, t)).unwrap()).le(1*x_target);
            }
        }

        // forbidden scenarios
        for scenario in forbidden {
            let mut y_vec: Vec<LpExpression> = Vec::new();
            for (i, j) in scenario {
                y_vec.push(1*yij.get(&(i, j)).unwrap());
            }
            if !y_vec.is_empty() {
                problem += lp_sum(&y_vec).le(scenario.len() as i32 - 1);
            }

        }

        // cost is optimal
        let mut cost_vec: Vec<LpExpression> = Vec::new();
        for (&(&i, &j, t), a) in &aijt {
            let cost = self.cost.get(&(i, j)).unwrap();
            cost_vec.push(*cost * a);
        }
        problem += lp_sum(&cost_vec).le(1.0*optimal_value);

        //println!("\nProblem: {}\n", problem.to_lp_file_format());

        let solver = CbcSolver::new();
        match solver.run(&problem) {
            Ok((status, res)) => {
                // Read y's into list
                let mut scenario : HashMap<(usize, usize), i32> = HashMap::new();
                for ((&i, &j), y) in yij {
                    if let Some(yij) = res.get(&y.name) {
                        scenario.insert((i, j), *yij as i32);
                    }
                }
                return Ok((problem.to_lp_file_format(), status, scenario))

            }
            Err(msg) => return Err(msg)
        }
    }


    fn build_and_solve_largest_obstruction(&self, forbidden: &Vec<Vec<(usize, usize)>>) -> Result<(String, Status, HashMap<(usize, usize), i32>), String> {
        let mut problem = LpProblem::new("Find Largest Obstruction", LpObjective::Maximize);

        // Build a_ij^t Variables
        let mut aijt = HashMap::new();
        for t in 0..self.trains.len() {
            for (i, j) in &self.edges {
                aijt.insert((i, j, t), LpBinary::new(&format!("a_{}_{}_{}", i, j, t)));
            }
        }

        // Build y_ij variables
        let mut yij = HashMap::new();
        for (i, j) in &self.edges {
            yij.insert((i, j), LpBinary::new(&format!("y_{}_{}", i, j)));
        }

        // build n
        let n = LpInteger::new("n");

        // Objective function
        problem += (1*&n);

        let mut obj_vec: Vec<LpExpression> = Vec::new();
        for (&(&i, &j), y) in &yij{
            obj_vec.push(1 * y);
        }
        problem += lp_sum(&obj_vec).equal(2*&n);

        // Flow conservation
        for (t, _) in &self.trains {
            for i in &self.nodes {
                let mut sum_1_vec: Vec<LpExpression> = Vec::new();
                let mut sum_2_vec: Vec<LpExpression> = Vec::new();
                for j in &self.nodes {
                    if self.edges.contains(&(*i, *j)){
                        sum_1_vec.push(1*aijt.get(&(i, j, *t)).unwrap());
                        sum_2_vec.push(1*aijt.get(&(j, i, *t)).unwrap());
                    }
                }

                let target: i32 =
                    if *i == self.trains.get(&t).unwrap().0 {
                        1
                    } else if *i == self.trains.get(&t).unwrap().1 {
                        -1
                    } else {
                        0
                    };

                if !&sum_1_vec.is_empty() {
                    problem += (lp_sum(&sum_1_vec) - lp_sum(&sum_2_vec)).equal(target)
                } else {
                    //println!("No Edges found for Node {}", i);
                }

            }
        }

        // Blocked Tracks are not used:
        for t in 0..self.trains.len() {
            for (i,j) in &self.edges {
                problem += (1*aijt.get(&(i, j, t)).unwrap()).le(1-1*yij.get(&(i, j)).unwrap())
            }
        }

        // Tracks are blocked both ways
        for (i, j) in &self.edges {
            problem += (1*yij.get(&(i, j)).unwrap()).equal(1*yij.get(&(j, i)).unwrap());
        }

        // a's follow the pre-defined routes
        for t in 0..self.trains.len() {
            for (i, j) in &self.edges {
                let x_target = *self.x.get(&t).unwrap().get(&(*i, *j)).unwrap() as i32;
                // let mut x_sum = 0;
                // for route in self.x.get(&t).unwrap().iter() {
                //     x_sum += *route.get(&(*i, *j)).unwrap();
                //     //println!("Train {}, Route {}, Edge {}-{} has value {}", t, )
                // }
                problem += (aijt.get(&(i, j, t)).unwrap()).le(1*x_target);
            }
        }

        // forbidden scenarios
        for scenario in forbidden {
            let mut y_vec: Vec<LpExpression> = Vec::new();
            for (i, j) in scenario {
                y_vec.push(1*yij.get(&(i, j)).unwrap());
            }
            if !y_vec.is_empty() {
                problem += lp_sum(&y_vec).le(scenario.len() as i32 - 1);
            }

        }

        //println!("\nProblem: {}\n", problem.to_lp_file_format());

        let solver = CbcSolver::new();
        match solver.run(&problem) {
            Ok((status, res)) => {
                // Read y's into list
                let mut scenario : HashMap<(usize, usize), i32> = HashMap::new();
                for ((&i, &j), y) in yij {
                    if let Some(yij) = res.get(&y.name) {
                        scenario.insert((i, j), *yij as i32);
                    }
                }
                return Ok((problem.to_lp_file_format(), status, scenario))

            }
            Err(msg) => return Err(msg)
        }
    }



    fn build_and_solve(&self, y: &HashMap<(usize, usize), i32>) -> Result<(String, Status, HashMap<String, f32>, f32), String>{

        let mut problem = LpProblem::new("One Problem", LpObjective::Minimize);

        // Build a_ij^t Variables
        let mut aijt = HashMap::new();
        for t in 0..self.trains.len() {
            for (i, j) in &self.edges {
                aijt.insert((i, j, t), LpBinary::new(&format!("a_{}_{}_{}", i, j, t)));
            }
        }

        // Objective function
        // let mut obj_vec: Vec<LpExpression> = Vec::new();
        // for (&(&i, &j, t), a) in &aijt {
        //     let cost = self.cost.get(&(i, j)).unwrap();
        //     obj_vec.push(*cost * a);
        // }
        // problem += lp_sum(&obj_vec);

        // Objective function without cost:
        problem += 1;


        // Flow conservation
        for (t, _) in &self.trains {
            for i in &self.nodes {
                let mut sum_1_vec: Vec<LpExpression> = Vec::new();
                let mut sum_2_vec: Vec<LpExpression> = Vec::new();
                for j in &self.nodes {
                    if self.edges.contains(&(*i, *j)){
                        sum_1_vec.push(1*aijt.get(&(i, j, *t)).unwrap());
                        sum_2_vec.push(1*aijt.get(&(j, i, *t)).unwrap());
                    }
                }

                let target: i32 =
                    if *i == self.trains.get(&t).unwrap().0 {
                        1
                    } else if *i == self.trains.get(&t).unwrap().1 {
                        -1
                    } else {
                        0
                    };

                if !&sum_1_vec.is_empty() {
                    problem += (lp_sum(&sum_1_vec) - lp_sum(&sum_2_vec)).equal(target)
                } else {
                    //println!("No Edges found for Node {}", i);
                }

            }
        }

        // Blocked Tracks are not used:
        for t in 0..self.trains.len() {
            for (i,j) in &self.edges {
                problem += (1*aijt.get(&(i, j, t)).unwrap()).le(1-1*y.get(&(*i, *j)).unwrap())
            }
        }

        // a's follow the pre-defined routes
        for t in 0..self.trains.len() {
            for (i, j) in &self.edges {
                let x_target = *self.x.get(&t).unwrap().get(&(*i, *j)).unwrap() as i32;
                // let mut x_sum = 0;
                // for route in self.x.get(&t).unwrap().iter() {
                //     x_sum += *route.get(&(*i, *j)).unwrap();
                //     //println!("Train {}, Route {}, Edge {}-{} has value {}", t, )
                // }
                problem += (aijt.get(&(i, j, t)).unwrap()).le(1*x_target);
            }
        }

        //println!("\nProblem: {}\n", problem.to_lp_file_format());

        let solver = CbcSolver::new();
        match solver.run(&problem) {
            Ok((status, res)) => {
                // for (name, value) in res.iter() {
                //     println!("value of {} = {}", name, value);
                // }

                // Calculate objective value:
                let mut value = 0.0;
                for ((&i, &j, t), a) in aijt {
                    let c = self.cost.get(&(i, j)).unwrap();

                    if let Some(a_i_j_t) = res.get(&a.name) {
                        value += c * a_i_j_t;
                    }
                }
                return Ok((problem.to_lp_file_format(), status, res, value))

            }
            Err(msg) => return Err(msg)
        }
    }

}

fn is_in_superset(root: &HashMap<(usize, usize), i32>, candidate: &HashMap<(usize, usize), i32>) -> bool {
    let mut all_in_candidate = true;

    for (edge, value) in root {
        if *value == 1  {
            all_in_candidate = *candidate.get(edge).unwrap() == 1;
        }
    }

    all_in_candidate
}

fn scenario_to_obstruction_list(scenario: HashMap<(usize, usize), i32>) -> Vec<(usize, usize)> {
    let mut obstruction_list : Vec<(usize, usize)> = vec![];

    for (edge, value) in scenario {
        if value == 1 { obstruction_list.push(edge);}
    }
    obstruction_list
}

/// Generates the power set of a vector
/// Thankfully taken from https://users.rust-lang.org/t/power-set-of-a-vec/29874/5
fn power_set<T: Clone>(a: &Vec<T>) -> Vec<Vec<T>> {
    a.iter().fold(vec![vec![]], |mut p, x| {
        let i = p.clone().into_iter()
            .map(|mut s| {s.push(x.clone()); s});
        p.extend(i); p})
}

#[test]
fn threenodes() {
    let nodes = vec![1, 2, 3];
    let edges = vec![(1, 2), (2, 1), (1, 3), (3, 1), (2, 3), (3, 2)];
    let cost: HashMap<(usize, usize), f32> = vec![
        ((1, 2), 2.0),
        ((2, 1), 2.0),
        ((1, 3), 6.0),
        ((3, 1), 6.0),
        ((2, 3), 3.0),
        ((3, 2), 3.0)
    ].into_iter().collect();

    let y: HashMap<(usize, usize), i32> = vec![
        ((1, 2), 0),
        ((2, 1), 0),
        ((1, 3), 0),
        ((3, 1), 0),
        ((2, 3), 1),
        ((3, 2), 1),
    ].into_iter().collect();

    let x_0 : HashMap<(usize, usize), i32> = vec![
        ((1, 2), 1),
        ((2, 1), 0),
        ((1, 3), 0),
        ((3, 1), 0),
        ((2, 3), 1),
        ((3, 2), 0)
    ].into_iter().collect();

    let x_1 : HashMap<(usize, usize), i32> = vec![
        ((1, 2), 0),
        ((2, 1), 0),
        ((1, 3), 1),
        ((3, 1), 0),
        ((2, 3), 0),
        ((3, 2), 0)
    ].into_iter().collect();

    let routes_0 = vec![x_0, x_1];

    let mut x : HashMap<usize, Vec<HashMap<(usize, usize), i32>>> = HashMap::new();
    x.insert(0, routes_0);

    let trains = vec![(1, 3)];

    let verifier = NetVerifier::new(nodes, edges, cost, x, trains, 2);

    //verifier.build_and_solve_cbc(&y);
}

#[test]
fn fournodes() {
    let nodes = vec![1, 2, 3, 4];
    let edges = vec![(1, 2), (2, 1), (1, 3), (3, 1), (2, 3), (3, 2), (3, 4), (4, 3), (2, 4), (4, 2)];
    let cost: HashMap<(usize, usize), f32> = vec![
        ((1, 2), 2.0),
        ((2, 1), 2.0),
        ((1, 3), 5.0),
        ((3, 1), 5.0),
        ((2, 3), 2.0),
        ((3, 2), 2.0),
        ((3, 4), 10.0),
        ((4, 3), 10.0),
        ((2, 4), 1.0),
        ((4, 2), 1.0),
    ].into_iter().collect();

    let y: HashMap<(usize, usize), i32> = vec![
        ((1, 2), 0),
        ((2, 1), 0),
        ((1, 3), 0),
        ((3, 1), 0),
        ((2, 3), 1),
        ((3, 2), 1),
        ((2, 4), 0),
        ((4, 2), 0),
        ((3, 4), 0),
        ((4, 3), 0),
    ].into_iter().collect();

    let x_0_0 : HashMap<(usize, usize), i32> = vec![
        ((1, 2), 1),
        ((2, 1), 0),
        ((1, 3), 0),
        ((3, 1), 0),
        ((2, 3), 1),
        ((3, 2), 0),
        ((2, 4), 0),
        ((4, 2), 0),
        ((3, 4), 0),
        ((4, 3), 0),
    ].into_iter().collect();

    let x_0_1 : HashMap<(usize, usize), i32> = vec![
        ((1, 2), 0),
        ((2, 1), 0),
        ((1, 3), 1),
        ((3, 1), 0),
        ((2, 3), 0),
        ((3, 2), 0),
        ((2, 4), 0),
        ((4, 2), 0),
        ((3, 4), 0),
        ((4, 3), 0),
    ].into_iter().collect();

    let routes_0 = vec![x_0_0, x_0_1];

    let x_1_0 : HashMap<(usize, usize), i32> = vec![
        ((1, 2), 0),
        ((2, 1), 0),
        ((1, 3), 0),
        ((3, 1), 0),
        ((2, 3), 0),
        ((3, 2), 0),
        ((2, 4), 0),
        ((4, 2), 0),
        ((3, 4), 1),
        ((4, 3), 0),
    ].into_iter().collect();

    let x_1_1 : HashMap<(usize, usize), i32> = vec![
        ((1, 2), 0),
        ((2, 1), 0),
        ((1, 3), 0),
        ((3, 1), 0),
        ((2, 3), 0),
        ((3, 2), 1),
        ((2, 4), 1),
        ((4, 2), 0),
        ((3, 4), 0),
        ((4, 3), 0),
    ].into_iter().collect();

    let routes_1 = vec![x_1_0, x_1_1];

    let mut x : HashMap<usize, Vec<HashMap<(usize, usize), i32>>> = HashMap::new();
    x.insert(0, routes_0);
    x.insert(1, routes_1);

    let trains = vec![(1, 3), (3, 4)];

    let verifier = NetVerifier::new(nodes, edges, cost, x, trains, 4);

    verifier.find_worst_edge();

}


